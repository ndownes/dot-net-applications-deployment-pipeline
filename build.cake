#tool nuget:?package=xunit.runner.console

#load build/paths.cake

var target = Argument("Target", "Build");
var configuration = Argument("Configuration", "Release");
var packageOutputPath = Argument<DirectoryPath>("PackageOutputPath", "packages");
var nugetPackageVersion = Argument("NugetPackageVersion");

Task("Restore")
    .Does(() =>
{
    NuGetRestore(Paths.SolutionFile);
});

Task("Build")
    .IsDependentOn("Restore")
    .Does(() =>
{
    DotNetBuild(Paths.SolutionFile, settings => settings.SetConfiguration(configuration).WithTarget("Build"));
});

Task("Test")
    .IsDependentOn("Build")
    .Does(() => {
        XUnit2($"**/bin/{configuration}/*Tests.dll");
    });

Task("Remove-Packages")
	.Does(() => {
			CleanDirectory(packageOutputPath);
		});

Task("Package-Nuget")
	.IsDependentOn("Test")    
    .IsDependentOn("Remove-Packages")
    .Does(() => {
        EnsureDirectoryExists(packageOutputPath);

        NuGetPack(Paths.WebNuspecFile, 
            new NuGetPackSettings {
                Version = nugetPackageVersion,
                OutputDirectory = packageOutputPath,
                NoPackageAnalysis = true
        });
    });

RunTarget(target);