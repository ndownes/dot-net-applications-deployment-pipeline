﻿public static class Paths
{
	public static FilePath SolutionFile => "src/Web/Todo.Web.sln";
	public static FilePath WebNuspecFile => "src/Web/Todo.Web/Todo.Web.nuspec";	
}